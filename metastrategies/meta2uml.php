<?php
/*

Copyright 2017

Grupo de Investigación en Lenguajes e Inteligencia Artificial (GILIA) -
Facultad de Informática
Universidad Nacional del Comahue

meta2uml.php

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

class MetaUML extends MetaStrategy{

	function __construct(){
		$this->uml = ["Object type" => [],
					   "Subsumption" => [],
					   "Role" => [],
					   "Association" => [],
					   "Object type cardinality" => [],
					   "Attribute" => []
					  ];
	}
	
	
    function create_model($json_str){
        $json = json_decode($json_str, true);
        
 //       print_r($json);

        $this->identifyClasses($json);
        $this->identifySubsumption($json);
		// $this->idenfityBinaryAssoc0NWithoutRoles($json);
		$this->idenfityBinaryAssoc($json);
         
    }
}



?>
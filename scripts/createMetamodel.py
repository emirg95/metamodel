import os
import re


def main():
    try:
        f = open('metamodel.txt', 'r')
        work_dir = '/home/emiliano'
        file_content = open('template.php', 'r').read()
        lines = f.readlines()
        for line in lines:
            if line.find('_class') != -1:
                new_file_name = work_dir + line.replace('_class\n', '.php')
                class_name = re.search('/([a-zA-Z]+?).php', new_file_name)
                if class_name != None:
                    class_name = class_name.group(1)
                # class_name = new_file_name[]
                if not os.path.exists(new_file_name) and class_name != None:
                    if not os.path.exists(os.path.dirname(new_file_name)):
                        os.makedirs(os.path.dirname(new_file_name))
                    with open(new_file_name, 'w+') as new_file:
                        new_file.write(file_content % (class_name,class_name.capitalize()))
    except Exception as e:
        print(e.message)
    finally:
        f.close()


if __name__ == '__main__':
    main()

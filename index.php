<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1> Metamodel </h1>
    <?php
        $json = json_decode('{"namespaces":{"ontologyIRI":[{"prefix":"crowd","value":"http://crowd.fi.uncoma.edu.ar#"}],"defaultIRIs":[{"prefix":"rdf","value":"http://www.w3.org/1999/02/22-rdf-syntax-ns#"},{"prefix":"rdfs","value":"http://www.w3.org/2000/01/rdf-schema#"},{"prefix":"xsd","value":"http://www.w3.org/2001/XMLSchema#"},{"prefix":"owl","value":"http://www.w3.org/2002/07/owl#"}],"IRIs":[]},"classes":[{"name":"http://crowd.fi.uncoma.edu.ar#Person","attrs":[{"name":"http://crowd.fi.uncoma.edu.ar#Age","datatype":"http://www.w3.org/2001/XMLSchema#integer"}],"methods":[],"position":{"x":286,"y":125}},{"name":"http://crowd.fi.uncoma.edu.ar#Dog","attrs":[{"name":"http://crowd.fi.uncoma.edu.ar#Race","datatype":"http://www.w3.org/2001/XMLSchema#integer"}],"methods":[],"position":{"x":622,"y":136}},{"name":"http://crowd.fi.uncoma.edu.ar#Class3","attrs":[],"methods":[],"position":{"x":266,"y":335}}],"links":[{"name":"http://crowd.fi.uncoma.edu.ar#r3","classes":["http://crowd.fi.uncoma.edu.ar#Person","http://crowd.fi.uncoma.edu.ar#Dog"],"multiplicity":["1..1","4..6"],"roles":["http://crowd.fi.uncoma.edu.ar#person","http://crowd.fi.uncoma.edu.ar#dog"],"type":"association"},{"name":"http://crowd.fi.uncoma.edu.ar#s1","parent":"http://crowd.fi.uncoma.edu.ar#Person","classes":["http://crowd.fi.uncoma.edu.ar#Class3"],"multiplicity":null,"roles":null,"type":"generalization","constraint":["disjoint","covering"],"position":{"x":321,"y":276}}],"owllink":["\n"]}', true);
        
        $json_links = $json["links"];
        $rest = array_filter($json_links,function($gen){return $gen["type"] == "association";});
        
        foreach ($rest as $assoc){

            // Create the roles to push them into the Metamodel and the relationship
            $first_role = new Role($assoc["name"], $assoc["roles"][0]);
            $second_role = new Role($assoc["name"], $assoc["roles"][1]);
            $rel_roles = array($assoc["roles"][0], $assoc["roles"][1]);
            array_push($this->meta["Role"],$first_role->get_json_array());
            array_push($this->meta["Role"],$second_role->get_json_array());


            $assoc_obj = new Relationship($assoc["name"], $assoc["classes"], $rel_roles);
            array_push($this->meta["Association"],$assoc_obj->get_json_array());
            
    		$type_card_obj = new Objecttypecardinality($assoc["name"], $assoc["multiplicity"]);
    		array_push($this->meta["Object type cardinality"],$type_card_obj->get_json_array());
    	
    	}

    ?>
</body>
</html>